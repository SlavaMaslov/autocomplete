﻿; (function () {

    $.widget("custom.Autocomplete", $.ui.autocomplete, {

        options: {
            autofocus: true,
            isCollapsed: false,
            minLength: 3,
            delay: 500,
            urlAjax: ""
        },

        // конструктор плагина.
        _create: function () {
            // вызовем в нем конструктор родительского плагина
            $.ui.autocomplete.prototype._create.call(this);

            this.resultList = document.createElement("ul");
            $(this.resultList).insertAfter(this.element);

            //console.log(this.resultList);
        },

        myMethodName: function (obj, array) {
            var name, phone;

            $(obj.resultList).html("");


            for(var i=0; i<array.length; i++) {
                $(obj.resultList).append("<li>" + array[i].GetPersonFullName + " - " + array[i].GetPersonPhoneNumber + "</li>");
            }
        },

        search: function (event, ui) {
            var input = this.element.val(),
                func = this.myMethodName,
                obj = this;


            if (input.length > 2) {
                $.ajax({
                    method: "POST",
                    url: this.options.urlAjax,
                    data: { search: input }
                })
                .done(function (msg) {
                    func(obj, msg);
                });
            }
            else
            {
                $(this.resultList).html("");
            }
        },

        // деструктор плагина
        destroy: function () {
            // вызовем в нем деструктор родительского плагина
            $.ui.autocomplete.prototype.destroy.call(this);
        },

    });

    $("#autocomplete").Autocomplete({
        urlAjax: "/Home/Search/"
    });
    

})();