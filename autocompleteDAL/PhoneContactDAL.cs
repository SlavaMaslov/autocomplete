﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Entities;

namespace autocompleteDAL
{
    public class PhoneContactDAL
    {
        private List<Contact> phoneContacts;

        public PhoneContactDAL()
        {
            this.phoneContacts = new List<Contact>();

            this.phoneContacts.Add(new Contact 
            { 
                Id = 1,
                CodeGroup = 987,
                PhoneNumber = 3282700,
                Name = "Svyatoslav",
                Surname = "Maslov",
                Fathersname = "Andreevich"
            });

            this.phoneContacts.Add(new Contact
            {
                Id = 2,
                CodeGroup = 987,
                PhoneNumber = 7777777,
                Name = "Petr",
                Surname = "Petrov",
                Fathersname = "Petrovich"
            });

            this.phoneContacts.Add(new Contact
            {
                Id = 3,
                CodeGroup = 987,
                PhoneNumber = 1111111,
                Name = "Ivan",
                Surname = "Ivanov",
                Fathersname = "Ivanovich"
            });

            this.phoneContacts.Add(new Contact
            {
                Id = 4,
                CodeGroup = 987,
                PhoneNumber = 3333333,
                Name = "Sidr",
                Surname = "Sidorov",
                Fathersname = "Sidorovich"
            });

            this.phoneContacts.Add(new Contact
            {
                Id = 5,
                CodeGroup = 987,
                PhoneNumber = 4444444,
                Name = "Vasiliy",
                Surname = "Vasiliev",
                Fathersname = "Vasilievich"
            });
        }

        public IEnumerable<Contact> getAllPhoneContacts()
        {
            return this.phoneContacts;
        }



        public Contact getPhoneContactByIndex(int Id)
        { 
            if( this.phoneContacts.Count >= Id )
            {
                return this.phoneContacts[Id];
            }
            return null;
        }

        public bool addPhoneContact(Contact contact)
        {
            this.phoneContacts.Add(contact);
            return true;
        }
    }
}
