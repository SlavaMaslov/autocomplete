﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using autocompleteBLL;
using Entities.Entities;
using System.Text.RegularExpressions;

namespace autocomplete.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public JsonResult Search(string search)
        {
            search = search.Trim();

            PhoneContactBLL modelBLL = new PhoneContactBLL();
            List<Contact> contacts = modelBLL.findContacts(search).ToList();

            return Json(contacts);
        }

        public ActionResult Phone()
        {
            ViewBag.Message = "Lets find some phone contacts...";

            return View();
        }
    }
}