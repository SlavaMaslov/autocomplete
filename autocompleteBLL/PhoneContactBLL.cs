﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using autocompleteDAL;
using Entities.Entities;
using System.Text.RegularExpressions;

namespace autocompleteBLL
{
    public class PhoneContactBLL
    {
        public PhoneContactDAL PhoneContactDataSource;

        public PhoneContactBLL()
        {
            this.PhoneContactDataSource = new PhoneContactDAL();
        }

        public IEnumerable<Contact> findContacts( string value ) 
        {
            string pattern = "(\\s)?(\\+7|8)?([0-9 \\(\\)\\-]){3,}";
            string phone = "";
            string name = "";
            
            // ПОИСК В ПОЛУЧЕННОЙ СТРОКЕ ЧЕГО-ТО ПОХОЖЕГО НА ТЕЛЕФОН
            Match m = Regex.Match(value, pattern, RegexOptions.IgnorePatternWhitespace);
            if (m.Success)
            {
                phone = m.Value;
                string[] phoneParts = phone.Split(new Char[] { ' ', '(', ')', '-', '+' }, StringSplitOptions.RemoveEmptyEntries);
                phone = string.Join("", phoneParts);
                if (phone.Length == 11 && phone[0] == '7')
                {
                    phone = "8" + phone.Substring(1);
                }
            }

            // ОБРАБОТКА ВСЕГО ОСТАЛЬНОГО, ЧТО НЕ ПОХОЖЕ НА ТЕЛЕФОН
            name = Regex.Replace(value, pattern, " ");
            string[] nameParts = name.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            // ВЫБОР СООТВЕТСТВУЮЩЕГО МЕТОДА ФИЛЬТРАЦИИ
            List<Contact> filteredContacts = new List<Contact>();
            List<Contact> contactList = this.PhoneContactDataSource.getAllPhoneContacts().ToList();
            if (nameParts.Length > 0 && !String.IsNullOrEmpty(phone))
            {
                filteredContacts = contactList.FindAll(contact => contact.findPersonByFullNameOrPhoneNumber(phone, nameParts) == true);
            }
            else if (nameParts.Length == 0 && !String.IsNullOrEmpty(phone))
            {
                filteredContacts = contactList.FindAll(contact => contact.checkContactByPhone(phone) == true);
            }
            else if (nameParts.Length > 0 && String.IsNullOrEmpty(phone))
            {
                filteredContacts = contactList.FindAll(contact => contact.checkContactByFullName(nameParts) == true);
            }

            return filteredContacts;
        } 
 
    }
}
