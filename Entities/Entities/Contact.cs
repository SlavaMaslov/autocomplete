﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Entities.Entities
{
    public class Contact
    {
        public int Id { get; set; }
        public int CodeGroup {get; set;}
        public int PhoneNumber { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Fathersname { get; set; }

        public string GetPersonPhoneNumber
        {
            get
            {
                return "8" + this.CodeGroup + this.PhoneNumber;
            }
        }

        public string GetPersonFullName
        {
            get
            {
                return this.Surname + " " + this.Name + " " + this.Fathersname;
            }
        }

        public bool findPersonByFullNameOrPhoneNumber(string phone, string[] parts)
        {
            return this.checkContactByFullName(parts) && this.checkContactByPhone(phone);
        }

        public bool checkContactByFullName(string[] array)
        {
            bool result = true;
            string fullName = this.GetPersonFullName;

            foreach(var item in array)
            {
                if (fullName.IndexOf(item, StringComparison.CurrentCultureIgnoreCase) == -1)
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        public bool checkContactByPhone(string phone)
        {
            return this.GetPersonPhoneNumber.Contains(phone);
        }
    }
}